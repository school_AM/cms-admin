import { AUTH_LOGIN, AUTH_LOGOUT, AUTH_ERROR, AUTH_CHECK, AUTH_GET_PERMISSIONS} from "react-admin";

export default (type, params) => {
  
  if (type === AUTH_LOGIN) {
    
    const { username, password } = params;
    
    const request = new Request(process.env.REACT_APP_BACKEND_URL + "/auth/login", {
      method: "POST",
      body: JSON.stringify({ email: username, password }),
      headers: new Headers({ "Content-Type": "application/json" }),
    })
    
    return fetch(request)
      .then((response) => {
        if (response.status < 200 || response.status >= 300) {
          throw new Error(response.statusText);
        }
        return response.json();
      })
      .then(({ token, roles }) => {
        localStorage.setItem("token", token);
        localStorage.setItem("roles", roles);
      });
  }
  
  if (type === AUTH_LOGOUT) {
    localStorage.removeItem("token");
    return Promise.resolve();
  }
  
  if (type === AUTH_ERROR) {
    const status  = params.status;
    if (status === 401) {
      localStorage.removeItem("token");
      return Promise.reject("");
    }
    return Promise.resolve();
  }
  
  if (type === AUTH_CHECK) {
    return localStorage.getItem("token") ? Promise.resolve() : Promise.reject("");
  }
  
  if (type === AUTH_GET_PERMISSIONS) {
    const roles = localStorage.getItem("roles");
    return roles ? Promise.resolve(roles) : Promise.reject();
  }
  
  return Promise.resolve();
}
