/**
 * Convert a `File` object returned by the upload input into a base 64 string.
 * That's not the most optimized way to store images in production, but it's
 * enough to illustrate the idea of data provider decoration.
 */
import {async} from "q";

const convertFileToBase64 = (file) => new Promise((resolve, reject) => {
  const reader = new FileReader();
  reader.readAsDataURL(file.rawFile);
  
  reader.onload = () => resolve(reader.result);
  reader.onerror = reject;
});

const uploadImage = async (data: { rawFile: File, src: string }, url: string) => {
  const blob = await fetch(data.src).then((r) => r.blob());
  
  return new Promise((resolver, rejecter) => {
    const xhr = new XMLHttpRequest();
    
    xhr.onload = () => {
      if (xhr.status < 400) {
        resolver(true)
      } else {
        const error = new Error(xhr.response);
        rejecter(error)
      }
    };
    xhr.onerror = (error) => {
      rejecter(error)
    };
    
    xhr.open("PUT", url);
    xhr.setRequestHeader("Content-Type", data.rawFile.type);
    xhr.send(blob);
  })
}

const addUploadFeature = (requestHandler) => (type, resource, params) => {
  
  if ((type === "UPDATE" || type === "CREATE") && resource === "formats") {
  
    if (params.data.files) {
      
      const file = params.data.files
      
      return requestHandler(type, resource, {
        ...params,
        data: {
          ...params.data,
          s3Key: file.title,
          size: file.rawFile.size,
          files: undefined,
        },
      })
        .then((result) => {
          return uploadImage(file, result.data.uploadUrl)
            .then((_) => result)
        })
    }
  }
  
  // for other request types and resources, fall back to the default request handler
  return requestHandler(type, resource, params);
};

export default addUploadFeature;
