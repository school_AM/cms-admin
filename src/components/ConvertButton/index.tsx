import React, {Component} from "react";
import {Button} from "@material-ui/core";
// @ts-ignore
import {connect} from "react-redux"
import {fetchStart, fetchEnd} from "ra-core/lib/actions/fetchActions";

const host = process.env.REACT_APP_BACKEND_URL!

interface IProps {
  record: any
  fetchStart(): void
  fetchEnd(): void
}

class ConvertButtonClass extends Component<IProps, {}> {
  
  public handleClick = () => {
    this.props.fetchStart()
    convert(this.props.record.id)
      .then(() => {
        console.log("Convert done")
      })
      .catch((e) => {
        console.error("Convert fail", e)
      })
      .finally(() => {
        this.props.fetchEnd()
      })
  }
  
  public render() {
    return <Button onClick={this.handleClick}>Convert to .gltf</Button>;
  }
}

const convert = async (modelId: string) => {
  
  const token = localStorage.getItem("token");
  
  return new Promise((resolver, rejecter) => {
    const xhr = new XMLHttpRequest();
    
    xhr.onload = () => {
      if (xhr.status < 400) {
        resolver(true)
      } else {
        const error = new Error(xhr.response);
        rejecter(error)
      }
    };
    xhr.onerror = (error) => {
      rejecter(error)
    };
    
    xhr.open("POST", `${host}/models/${modelId}/convert/to-gltf`);
    xhr.setRequestHeader("Authorization", `Bearer ${token}`);
    xhr.send();
  })
}

export const ConvertButton = connect(null, {fetchStart, fetchEnd})(ConvertButtonClass)
