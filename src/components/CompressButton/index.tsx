import React, {Component} from "react";
import {Button} from "@material-ui/core";
// @ts-ignore
import {connect} from "react-redux"
import {fetchStart, fetchEnd} from "ra-core/lib/actions/fetchActions";

const host = process.env.REACT_APP_BACKEND_URL!

interface IProps {
  record: any
  fetchStart(): void
  fetchEnd(): void
}

class CompressButtonClass extends Component<IProps, {}> {
  
  public handleClick = () => {
    this.props.fetchStart()
    convert(this.props.record.id)
      .then(() => {
        console.log("Compress done")
      })
      .catch((e) => {
        console.error("Compress fail", e)
      })
      .finally(() => {
        this.props.fetchEnd()
      })
  }
  
  public render() {
    return <Button onClick={this.handleClick}>Compress</Button>;
  }
}

const convert = async (modelId: string) => {
  
  const token = localStorage.getItem("token");
  
  return new Promise((resolver, rejecter) => {
    const xhr = new XMLHttpRequest();
    
    xhr.onload = () => {
      if (xhr.status < 400) {
        resolver(true)
      } else {
        const error = new Error(xhr.response);
        rejecter(error)
      }
    };
    xhr.onerror = (error) => {
      rejecter(error)
    };
    
    xhr.open("POST", `${host}/models/${modelId}/compress`);
    xhr.setRequestHeader("Authorization", `Bearer ${token}`);
    xhr.send();
  })
}

export const CompressButton = connect(null, {fetchStart, fetchEnd})(CompressButtonClass)
