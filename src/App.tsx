import React from "react";
import {Admin, EditGuesser, ListGuesser, Resource, ShowGuesser} from "react-admin";
import jsonServerProvider from "./providers/rest/jsonServer";
import "./App.css";
import {ModelCreate, ModelEdit, ModelList, ModelShow} from "./routes/models";
import {ApiKeyCreate, ApiKeyEdit, ApikeyList} from "./routes/apikeys";
import {UserCreate, UserEdit, UserList} from "./routes/users";
import authProvider from "./providers/auth/authProvider";
import {httpClient} from "./providers/auth/httpClient";
import {GroupCreate, GroupEdit, GroupList} from "./routes/groups";
import {isAdmin} from "./utils/auth";
import {FormatCreate, FormatShow} from "./routes/formats";
import {createMuiTheme} from "@material-ui/core/es";
import {grey, indigo, red} from "@material-ui/core/es/colors";
import JssProvider from "react-jss/lib/JssProvider";
import addUploadFeature from "./providers/upload/uploadProvider";

const dataProvider = addUploadFeature(jsonServerProvider(process.env.REACT_APP_BACKEND_URL, httpClient));

const myTheme = createMuiTheme({
  palette: {
    primary: indigo,
    secondary: grey,
    error: red,
  },
});

const App = () =>
  <JssProvider>
    <Admin
      theme={myTheme}
      dataProvider={dataProvider}
      authProvider={authProvider}
    >
      {(permissions) =>
        [
          isAdmin(permissions)
            ? <Resource key="groups" name="groups" list={GroupList} edit={GroupEdit} create={GroupCreate}/>
            : null,
          isAdmin(permissions)
            ? <Resource key="users" name="users" list={UserList} edit={UserEdit} create={UserCreate}/>
            : null,
          <Resource key="models" name="models" list={ModelList} show={ModelShow} edit={ModelEdit} create={ModelCreate}/>,
          <Resource key="formats" name="formats" create={FormatCreate} show={FormatShow} />,
          <Resource key="apikeys" name="apikeys" list={ApikeyList} edit={ApiKeyEdit} create={ApiKeyCreate}/>,
        ]}
    </Admin>
  </JssProvider>;

export default App;
