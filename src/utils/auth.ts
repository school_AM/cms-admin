import {UserRole} from "../model/userRole";

export const isAdmin = (permissions: any) => permissions && permissions.split(",").includes(UserRole.ADMIN)
