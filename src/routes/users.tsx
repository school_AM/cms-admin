import React, {cloneElement} from "react";
import {
  Create,
  Edit,
  FileField,
  FileInput,
  SimpleForm,
  TextInput,
  SelectArrayInput,
  List,
  Datagrid,
  TextField,
  EmailField,
  ReferenceField,
  ReferenceInput,
  SelectInput,
  DisabledInput,
  ArrayField,
  SingleFieldList,
  ChipField} from "react-admin"

export const UserList = (props) => (
  <List {...props}>
    <Datagrid rowClick="edit">
      
      <TextField source="id" />
      <TextField source="name" />
      <EmailField source="email" />
      
      <ArrayField source="roles">
        <SingleFieldList>
          <StringToLabelObject>
            <ChipField source={"label"}/>
          </StringToLabelObject>
        </SingleFieldList>
      </ArrayField>
      
      <ReferenceField source="groupId" reference="groups"><TextField source="id" /></ReferenceField>
      
    </Datagrid>
  </List>
);

export const StringToLabelObject = ({ record, children, ...rest }: any) =>
  cloneElement(children, {
    record: { label: record },
    ...rest,
  })

export const UserEdit = (props) => (
    <Edit {...props}>
      <SimpleForm redirect={"list"}>
        <DisabledInput source="id"/>
  
        <ReferenceInput label="Group" source="groupId" reference="groups">
          <SelectInput optionText="name" />
        </ReferenceInput>
        
        <TextInput source="name" />
        <TextInput source="email" type={"email"} />
        <SelectArrayInput
          source="roles"
          choices={[
            { id: "ADMIN", name: "Admin" },
            { id: "USER", name: "User" },
          ]}
        />
      </SimpleForm>
    </Edit>
);

export const UserCreate = (props) => (
    <Create  {...props}>
      <SimpleForm redirect={"list"}>
  
        <ReferenceInput label="Group" source="groupId" reference="groups">
          <SelectInput optionText="name" />
        </ReferenceInput>
        
        <TextInput source="name" />
        <TextInput source="email" type={"email"} />
        <TextInput source="password" type={"password"}/>
        <SelectArrayInput
          source="roles"
          choices={[
            { id: "ADMIN", name: "Admin" },
            { id: "USER", name: "User" },
          ]}
        />
      </SimpleForm>
    </Create>
);
