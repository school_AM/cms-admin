import React from "react";
import {
  Create,
  Datagrid,
  DisabledInput,
  Edit,
  FileField,
  FileInput,
  List,
  ReferenceField,
  ReferenceInput,
  SelectInput,
  Show,
  SimpleForm,
  SimpleShowLayout,
  TextField,
  TextInput,
  CardActions,
  EditButton,
  DeleteButton,
} from "react-admin"
import {isAdmin} from "../utils/auth";
import {FormatList} from "./formats";
import {ConvertButton} from "../components/ConvertButton";
import {CompressButton} from "../components/CompressButton";

export const ModelList = ({ permissions, ...props }) => (
  <List {...props}>
    <Datagrid rowClick="show" expand={<FormatList />}>
      <TextField source="id" />
      <TextField source="name" />
      
      {isAdmin(permissions)
      && <ReferenceField source="groupId" reference="groups"><TextField source="id" /></ReferenceField> }
    </Datagrid>
  </List>
);

export const ModelShow = (props) => {
  
  // @ts-ignore
  const actions = <PostShowActions/>
  return (
    <Show {...props} actions={actions}>
      <SimpleShowLayout>
        <TextField source="id"/>
        <TextField source="name"/>
      </SimpleShowLayout>
    </Show>
  );
};

const PostShowActions = ({ basePath, data, resource }) => (
  <CardActions >
    <ConvertButton record={data} />
    <CompressButton record={data} />
    <EditButton basePath={basePath} record={data} />
    <DeleteButton basePath={basePath} record={data} />
  </CardActions>
);

export const ModelEdit = ({ permissions, ...props }) => (
    <Edit {...props}>
      <SimpleForm redirect={"list"}>
        
        <DisabledInput source="id"/>
        
        {isAdmin(permissions)
        
        && <ReferenceInput label="Group" source="groupId" reference="groups">
          <SelectInput optionText="name" />
        </ReferenceInput>}
        
        <TextInput source="name" />
        
      </SimpleForm>
    </Edit>
);

export const ModelCreate = ({ permissions, ...props }) => (
    <Create  {...props}>
      <SimpleForm redirect={"list"}>
        <TextInput source="name" />
  
        {isAdmin(permissions)
        && <ReferenceInput label="Group" source="groupId" reference="groups">
          <SelectInput optionText="name" />
        </ReferenceInput>}
        
      </SimpleForm>
    </Create>
);
