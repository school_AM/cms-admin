import React from "react";
import {
  Create,
  Edit,
  SimpleForm,
  TextInput,
  List,
  Datagrid,
  TextField,
  ReferenceField,
  ReferenceInput,
  SelectInput,
  DisabledInput} from "react-admin"
import {isAdmin} from "../utils/auth";

export const ApikeyList = ({ permissions, ...props }) => (
  <List {...props}>
    <Datagrid rowClick="edit">
      <TextField source="id" />
      <TextField source="name" />
  
      {isAdmin(permissions)
      && <ReferenceField source="groupId" reference="groups"><TextField source="id" /></ReferenceField>}
      
    </Datagrid>
  </List>
);

export const ApiKeyEdit = ({ permissions, ...props }) => (
    <Edit {...props}>
      <SimpleForm redirect={"list"} >
        <DisabledInput source="id"/>
        
        {isAdmin(permissions)
        && <ReferenceInput label="Group" source="groupId" reference="groups">
          <SelectInput optionText="name" />
        </ReferenceInput>}
        
        <DisabledInput source="value" />
        <TextInput source="name" />
      </SimpleForm>
    </Edit>
);

export const ApiKeyCreate = ({ permissions, ...props }) => (
    <Create  {...props}>
      <SimpleForm redirect={"edit"}>
        <DisabledInput source="value" />
        <TextInput source="name" />
        
        {isAdmin(permissions)
        && <ReferenceInput label="Group" source="groupId" reference="groups">
          <SelectInput optionText="name" />
        </ReferenceInput>}
        
      </SimpleForm>
    </Create>
);
