import React from "react";
import {
  Create,
  Edit,
  SimpleForm,
  TextInput,
  List,
  Datagrid,
  TextField,
  DisabledInput} from "react-admin"

export const GroupList = (props) => (
  <List {...props}>
    <Datagrid rowClick="edit">
      <TextField source="id" />
      <TextField source="name" />
    </Datagrid>
  </List>
);

export const GroupEdit = (props) => (
  <Edit {...props}>
    <SimpleForm redirect={"list"}>
      <DisabledInput source="id"/>
      <TextInput source="name" />
    </SimpleForm>
  </Edit>
)

export const GroupCreate = (props) => (
  <Create {...props}>
    <SimpleForm redirect={"list"}>
      <TextInput source="name" />
    </SimpleForm>
  </Create>
)
