import * as React from "react";
import { Link } from "react-router-dom";
import {
  Show,
  SimpleShowLayout,
  TextField,
  ReferenceManyField,
  Datagrid,
  DateField,
  DeleteButton,
  Button,
  Create,
  SimpleForm,
  ReferenceInput,
  ReferenceField,
  TextInput,
  SelectInput,
  required,
  Edit,
  DisabledInput,
  FileInput,
  FileField} from "react-admin"
import {parse} from "querystring";

export const FormatList = (props) => (
  <Show
    {...props}
    title={" "}
  >
    <SimpleShowLayout>
      <ReferenceManyField
        label="Formats"
        reference="formats"
        target="modelId"
      >
        <Datagrid rowClick="show" >
          <TextField source="id" />
          <TextField source="extension" />
          <TextField source="s3Key" title="Title" />
          <TextField source="size" title="Size" />
          <DeleteButton redirect={false}/>
        </Datagrid>
      </ReferenceManyField>
      <AddButton record={props.record}/>
    </SimpleShowLayout>
  </Show>
)

export const AddButton = ({ record }) => (
  <Button
    component={Link}
    to={`/formats/create?modelId=${record.id}`}
    label="Add a format"
    title="Add a format"
  />
)

export const FormatCreate = (props) => {
  
  // Read the post_id from the location which is injected
  // by React Router and passed to our component by react-admin automatically
  const { modelId: modelIdString } = parse(props.location.search);
  const modelId = modelIdString ? modelIdString as string : "";
  
  const redirect = modelId ? `/models/${modelId}/show` : false;
  
  return (
    <Create {...props}>
      <SimpleForm
        defaultValue={{ modelId }}
        redirect={redirect}
      >
        
        <ReferenceInput
          source="modelId"
          reference="models"
          allowEmpty={false}
          validate={required()}
        >
          <SelectInput optionText="name" />
        </ReferenceInput>
        
        <FileInput source="files" label="Model files" multiple={false}>
          <FileField source="src" title="title" />
        </FileInput>
        
      </SimpleForm>
    </Create>
  );
}

export const FormatShow = (props) => (
  <Show
    {...props}
    title={" "}
  >
    <SimpleShowLayout>
      <ReferenceField label="Model" source="modelId" reference="models">
        <TextField source="name" />
      </ReferenceField>
      <TextField source="id" />
      <TextField source="extension" />
      <TextField source="s3Key" title="Title" />
      <TextField source="size" title="Size" />
      <TextField source="url" title="Url" />
    </SimpleShowLayout>
  </Show>
)
